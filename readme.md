## Beeeeers app on top of [Punk api v2](https://punkapi.com/documentation/v2)

As architecture it uses `Clean Architecture` with `MPV`, and with such connection can work both in offline and online modes.

Contains 2 activities:
* List of beers
    * shows basic beer info (name, image and some properties)
    * show all or only favorite beers
    * sort by ABV, IBU, EBC
* Beer details
    * shows more detailed beer information
    * add or remove beer from favorite list

### Screenshots
Details
![Details](/screenshots/details.png)
List
![List](/screenshots/list.png)
Options
![Options](/screenshots/options.png)
Sort options
![Sort Options](/screenshots/sort_options.png)
