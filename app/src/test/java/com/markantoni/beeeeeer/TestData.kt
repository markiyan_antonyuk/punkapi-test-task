package com.markantoni.beeeeeer

import com.markantoni.beeeeeer.model.Beer

/** Test data */
val BEER_1 = Beer(1, "beer1", 1f, 2f, 3f)
val BEER_2 = Beer(2, "beer2", 2f, 3f, 1f)
val BEER_3 = Beer(3, "beer3", 3f, 1f, 2f)