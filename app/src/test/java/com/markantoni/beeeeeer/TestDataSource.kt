package com.markantoni.beeeeeer

import com.markantoni.beeeeeer.data.DataSource
import com.markantoni.beeeeeer.model.Beer

/** Test [DataSource] to be used in tests. It is empty and every test can override needed methods. */
open class TestDataSource : DataSource {
    override fun fetchAllBeers(onSuccess: (List<Beer>) -> Unit, onError: (Throwable) -> Unit) = Unit

    override fun fetchFavoriteBeers(onSuccess: (List<Beer>) -> Unit, onError: (Throwable) -> Unit) = Unit

    override fun fetchFavoriteStatus(beer: Beer, callback: (Boolean) -> Unit) = Unit

    override fun setFavorite(beer: Beer) = Unit

    override fun removeFromFavorites(beer: Beer) = Unit
}