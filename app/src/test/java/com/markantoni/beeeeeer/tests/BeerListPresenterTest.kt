package com.markantoni.beeeeeer.tests

import com.markantoni.beeeeeer.BEER_1
import com.markantoni.beeeeeer.BEER_2
import com.markantoni.beeeeeer.BEER_3
import com.markantoni.beeeeeer.TestDataSource
import com.markantoni.beeeeeer.model.Beer
import com.markantoni.beeeeeer.presentation.list.BeerListContract
import com.markantoni.beeeeeer.presentation.list.BeerListPresenter
import com.nhaarman.mockito_kotlin.*
import org.junit.Before
import org.junit.Test

class BeerListPresenterTest {
    private lateinit var view: BeerListContract.View

    @Before
    fun setUp() {
        view = mock {
            on { showOnlyFavorites() } doReturn false
        }
    }

    @Test
    fun testViewNotBound() {
        val presenter = BeerListPresenter(TestDataSource())
        presenter.fetchBeers()

        verifyNoMoreInteractions(view)
    }

    @Test
    fun testUnboundView() {
        val presenter = BeerListPresenter(TestDataSource())
        presenter.bindView(view)

        verify(view).showLoading()
        verify(view).showOnlyFavorites()

        presenter.unbindView()
        presenter.fetchBeers()

        verifyNoMoreInteractions(view)
    }

    @Test
    fun testSuccessfulResponse() {
        val presenter = BeerListPresenter(object : TestDataSource() {
            override fun fetchAllBeers(onSuccess: (List<Beer>) -> Unit, onError: (Throwable) -> Unit) {
                onSuccess(listOf(BEER_1))
            }
        })
        presenter.bindView(view)

        view.verifyLoading()
        verify(view).showBeers(listOf(BEER_1))
        verifyNoMoreInteractions(view)
    }

    @Test
    fun testEmptyResponse() {
        val presenter = BeerListPresenter(object : TestDataSource() {
            override fun fetchAllBeers(onSuccess: (List<Beer>) -> Unit, onError: (Throwable) -> Unit) {
                onSuccess(listOf())
            }
        })
        presenter.bindView(view)

        view.verifyLoading()
        verify(view).showNoData()
        verifyNoMoreInteractions(view)
    }

    @Test
    fun testErrorResponse() {
        val presenter = BeerListPresenter(object : TestDataSource() {
            override fun fetchAllBeers(onSuccess: (List<Beer>) -> Unit, onError: (Throwable) -> Unit) {
                onError(NullPointerException())
            }
        })
        presenter.bindView(view)

        view.verifyLoading()
        verify(view).showError(any())
        verifyNoMoreInteractions(view)
    }

    @Test
    fun testSorting() {
        val presenter = BeerListPresenter(object : TestDataSource() {
            override fun fetchAllBeers(onSuccess: (List<Beer>) -> Unit, onError: (Throwable) -> Unit) {
                onSuccess(listOf(BEER_1, BEER_2, BEER_3))
            }
        })
        presenter.bindView(view)

        view.verifyLoading()
        verify(view, atLeastOnce()).showBeers(listOf(BEER_1, BEER_2, BEER_3))
        verify(view, never()).showBeers(listOf(BEER_1, BEER_3, BEER_1))

        presenter.sortData(BeerListContract.SortType.ABV)
        verify(view, atLeastOnce()).showBeers(listOf(BEER_1, BEER_2, BEER_3))

        presenter.sortData(BeerListContract.SortType.IBU)
        verify(view, atLeastOnce()).showBeers(listOf(BEER_3, BEER_1, BEER_2))

        presenter.sortData(BeerListContract.SortType.EBC)
        verify(view, atLeastOnce()).showBeers(listOf(BEER_2, BEER_3, BEER_1))

        presenter.sortData(BeerListContract.SortType.NONE)
        verify(view, atLeastOnce()).showBeers(listOf(BEER_1, BEER_2, BEER_3))
    }

    @Test
    fun testFavoriteResponse() {
        val presenter = BeerListPresenter(object : TestDataSource() {
            override fun fetchAllBeers(onSuccess: (List<Beer>) -> Unit, onError: (Throwable) -> Unit) {
                onSuccess(listOf(BEER_1, BEER_2))
            }

            override fun fetchFavoriteBeers(onSuccess: (List<Beer>) -> Unit, onError: (Throwable) -> Unit) {
                onSuccess(listOf(BEER_1))
            }
        })
        presenter.bindView(view)

        view.verifyLoading()
        verify(view, atLeastOnce()).showBeers(listOf(BEER_1, BEER_2))

        whenever(view.showOnlyFavorites()).thenReturn(true)
        presenter.fetchBeers()

        view.verifyLoading()
        verify(view, atLeastOnce()).showBeers(listOf(BEER_1))
        verify(view, never()).showBeers(listOf(BEER_2))
        verifyNoMoreInteractions(view)
    }

    private fun BeerListContract.View.verifyLoading() {
        verify(this, atLeastOnce()).showLoading()
        verify(this, atLeastOnce()).showOnlyFavorites()
        verify(this, atLeastOnce()).hideLoading()
    }
}
