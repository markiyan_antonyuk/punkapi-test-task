package com.markantoni.beeeeeer.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * Main model class, ideally DB/WEB logic should be cut out of here and have their own models which are converted to this one.
 * But for demo, I didn't to that.
 */
@Parcelize
@Entity(tableName = "beers")
data class Beer(@PrimaryKey val id: Int, val name: String = "",
                val abv: Float = 0f, val ibu: Float = 0f, val ebc: Float = 0f,
                val description: String = "", @SerializedName("image_url") val imageUrl: String = "") : Parcelable