package com.markantoni.beeeeeer

import android.app.Application
import com.markantoni.beeeeeer.data.BeerDataSource

class BeerApp : Application() {
    override fun onCreate() {
        super.onCreate()
        BeerDataSource.init(this)
    }
}