package com.markantoni.beeeeeer.presentation.list

import com.markantoni.beeeeeer.model.Beer

interface BeerListContract {
    enum class SortType {
        NONE, ABV, IBU, EBC
    }

    interface Presenter {
        fun bindView(view: View)
        fun unbindView()
        fun fetchBeers()
        fun sortData(type: SortType)
    }

    interface View {
        fun showOnlyFavorites(): Boolean
        fun showLoading()
        fun hideLoading()
        fun showBeers(beers: List<Beer>)
        fun showNoData()
        fun showError(error: Throwable)
    }
}