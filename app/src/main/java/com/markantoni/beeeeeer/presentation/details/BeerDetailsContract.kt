package com.markantoni.beeeeeer.presentation.details

import com.markantoni.beeeeeer.model.Beer

interface BeerDetailsContract {
    interface View {
        val beer: Beer
        fun showFavorite(isFavorite: Boolean)
    }

    interface Presenter {
        fun bindView(view: View)
        fun unbindView()
        fun fetchFavoriteStatus()
        fun setFavorite()
        fun removeFromFavorites()
    }
}