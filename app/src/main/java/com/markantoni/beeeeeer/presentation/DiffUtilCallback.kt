package com.markantoni.beeeeeer.presentation

import android.support.v7.util.DiffUtil
import com.markantoni.beeeeeer.model.Beer


class DiffUtilCallback(private val oldList: List<Beer>, private val newList: List<Beer>) : DiffUtil.Callback() {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) = oldList[oldItemPosition] == newList[newItemPosition]

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    //contents won't change
    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) = areItemsTheSame(oldItemPosition, newItemPosition)
}