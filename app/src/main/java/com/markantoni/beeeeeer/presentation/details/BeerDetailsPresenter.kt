package com.markantoni.beeeeeer.presentation.details

import com.markantoni.beeeeeer.data.DataSource

class BeerDetailsPresenter(private val dataSource: DataSource) : BeerDetailsContract.Presenter {
    private var view: BeerDetailsContract.View? = null

    override fun bindView(view: BeerDetailsContract.View) {
        this.view = view
        fetchFavoriteStatus()
    }

    override fun unbindView() {
        view = null
    }

    override fun fetchFavoriteStatus() {
        view?.let { view ->
            dataSource.fetchFavoriteStatus(view.beer) { view.showFavorite(it) }
        }
    }

    override fun setFavorite() {
        view?.let {
            dataSource.setFavorite(it.beer)
            it.showFavorite(true)
        }
    }

    override fun removeFromFavorites() {
        view?.let {
            dataSource.removeFromFavorites(it.beer)
            it.showFavorite(false)
        }
    }
}