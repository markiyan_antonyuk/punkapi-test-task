package com.markantoni.beeeeeer.presentation.details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.markantoni.beeeeeer.R
import com.markantoni.beeeeeer.data.BeerDataSource
import com.markantoni.beeeeeer.loadImage
import com.markantoni.beeeeeer.model.Beer
import kotlinx.android.synthetic.main.activity_beer_details.*

class BeerDetailsActivity : AppCompatActivity(), BeerDetailsContract.View {

    companion object {
        private const val EXTRA_BEER = "extra.beer"
        fun newIntent(context: Context, beer: Beer) = Intent(context, BeerDetailsActivity::class.java).apply {
            putExtra(EXTRA_BEER, beer)
        }
    }

    override val beer by lazy { intent.getParcelableExtra<Beer>(EXTRA_BEER) ?: throw IllegalArgumentException("Need to provide beer") }
    private val presenter by lazy { BeerDetailsPresenter(BeerDataSource) }
    private var isFavorite = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_beer_details)
        beer.apply {
            beerDetailsImageIv.loadImage(imageUrl)
            beerDetailsTitleTv.text = name
            beerDetailsDescriptionTv.text = description
            beerDetailsAbvTv.text = getString(R.string.abv, abv)
            beerDetailsIbuTv.text = getString(R.string.ibu, ibu)
            beerDetailsEbcTv.text = getString(R.string.ebc, ebc)
            beerDetailsFavoriteLayout.setOnClickListener { if (isFavorite) presenter.removeFromFavorites() else presenter.setFavorite() }
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.bindView(this)
    }

    override fun onPause() {
        presenter.unbindView()
        super.onPause()
    }

    override fun showFavorite(isFavorite: Boolean) {
        this.isFavorite = isFavorite
        beerDetailsFavoriteBtn.isChecked = isFavorite
        beerDetailsFavoriteTv.setText(if (isFavorite) R.string.remove_from_favourites else R.string.add_to_favourites)
        beerDetailsFavoriteBtn.runPulseAnimation()
    }

    private fun View.runPulseAnimation() {
        animate().apply {
            cancel()
            scaleX(1.2f)
            scaleY(1.2f)
            duration = 200
            withEndAction {
                animate().apply {
                    cancel()
                    scaleX(1f)
                    scaleY(1f)
                    duration = 200
                    start()
                }
            }
            start()
        }
    }
}