package com.markantoni.beeeeeer.presentation.list

import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.markantoni.beeeeeer.R
import com.markantoni.beeeeeer.loadImage
import com.markantoni.beeeeeer.model.Beer
import com.markantoni.beeeeeer.presentation.DiffUtilCallback
import kotlinx.android.synthetic.main.view_holder_beer.view.*

class BeerListAdapter(private val onBeerSelected: (Beer) -> Unit) : RecyclerView.Adapter<BeerListAdapter.Holder>() {
    private var data = mutableListOf<Beer>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = Holder(LayoutInflater.from(parent.context)
            .inflate(R.layout.view_holder_beer, parent, false))

    override fun onBindViewHolder(holder: Holder, position: Int) = holder.bind(data[position])

    override fun getItemCount() = data.size

    fun updateBeers(beers: List<Beer>) {
        DiffUtil.calculateDiff(DiffUtilCallback(data, beers)).dispatchUpdatesTo(this)
        data = beers.toMutableList()
    }

    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(beer: Beer) {
            itemView.apply {
                beerListItemTitleTv.text = beer.name
                beerListItemImageIv.loadImage(beer.imageUrl)
                beerListItemAbvTv.text = context.getString(R.string.abv, beer.abv)
                beerListItemIbuTv.text = context.getString(R.string.ibu, beer.ibu)
                beerListItemEbcTv.text = context.getString(R.string.ebc, beer.ebc)
                beerListItemHolder.setOnClickListener { onBeerSelected(beer) }
            }
        }
    }
}
