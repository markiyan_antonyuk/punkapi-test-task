package com.markantoni.beeeeeer.presentation.list

import com.markantoni.beeeeeer.data.DataSource
import com.markantoni.beeeeeer.model.Beer

class BeerListPresenter(private val dataSource: DataSource) : BeerListContract.Presenter {
    private var view: BeerListContract.View? = null
    private var data: List<Beer>? = null
    private var sortType = BeerListContract.SortType.NONE

    override fun bindView(view: BeerListContract.View) {
        this.view = view
        fetchBeers()
    }

    override fun unbindView() {
        view = null
    }

    override fun fetchBeers() {
        view?.let { view ->
            view.showLoading()
            val onSuccess = { beers: List<Beer> ->
                view.hideLoading()
                data = beers
                handleData(beers)
            }
            val onError = { error: Throwable ->
                view.hideLoading()
                view.showError(error)
            }
            dataSource.apply {
                if (view.showOnlyFavorites()) {
                    fetchFavoriteBeers(onSuccess, onError)
                } else {
                    fetchAllBeers(onSuccess, onError)
                }
            }
        }
    }

    private fun handleData(data: List<Beer>) {
        view?.apply { if (data.isEmpty()) showNoData() else sortData(sortType) }
    }

    override fun sortData(type: BeerListContract.SortType) {
        sortType = type
        data?.let {
            view?.showBeers(it.sortedBy {
                when (type) {
                    BeerListContract.SortType.ABV -> it.abv
                    BeerListContract.SortType.IBU -> it.ibu
                    BeerListContract.SortType.EBC -> it.ebc
                    else -> it.id.toFloat()
                }
            })
        }
    }
}