package com.markantoni.beeeeeer.presentation.list

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.view.Menu
import android.view.MenuItem
import com.markantoni.beeeeeer.R
import com.markantoni.beeeeeer.data.BeerDataSource
import com.markantoni.beeeeeer.model.Beer
import com.markantoni.beeeeeer.presentation.details.BeerDetailsActivity
import com.markantoni.beeeeeer.setVisible
import com.markantoni.beeeeeer.toggle
import kotlinx.android.synthetic.main.activity_beer_list.*

class BeerListActivity : AppCompatActivity(), BeerListContract.View {
    private val presenter by lazy { BeerListPresenter(BeerDataSource) }
    private val snackbar by lazy { Snackbar.make(window.decorView, "", Snackbar.LENGTH_INDEFINITE) }

    private lateinit var adapter: BeerListAdapter
    private var isShowingFavorites = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_beer_list)
        beerListRecyclerView.apply {
            layoutManager = GridLayoutManager(this@BeerListActivity, 2)
            this@BeerListActivity.adapter = BeerListAdapter {
                startActivity(BeerDetailsActivity.newIntent(context, it))
            }
            adapter = this@BeerListActivity.adapter
        }
        beerListSwipeRefreshLayout.setOnRefreshListener { presenter.fetchBeers() }
    }

    override fun onResume() {
        super.onResume()
        presenter.bindView(this)
    }

    override fun onPause() {
        presenter.unbindView()
        super.onPause()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.beer_list, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val sort = { item: MenuItem, type: BeerListContract.SortType ->
            item.isChecked = true
            presenter.sortData(type)
            true
        }
        return when (item.itemId) {
            R.id.showFavorites -> {
                isShowingFavorites = !item.toggle()
                presenter.fetchBeers()
                true
            }
            R.id.sortByNone -> sort(item, BeerListContract.SortType.NONE)
            R.id.sortByAbv -> sort(item, BeerListContract.SortType.ABV)
            R.id.sortByIbu -> sort(item, BeerListContract.SortType.IBU)
            R.id.sortByEbc -> sort(item, BeerListContract.SortType.EBC)
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun showOnlyFavorites(): Boolean = isShowingFavorites

    override fun showLoading() {
        beerListProgressBar.setVisible(true)
        beerListRecyclerView.setVisible(false)
        beerListNoResultsTv.setVisible(false)
        snackbar.dismiss()
    }

    override fun hideLoading() {
        beerListSwipeRefreshLayout.isRefreshing = false
        beerListProgressBar.setVisible(false)
    }

    override fun showBeers(beers: List<Beer>) {
        beerListRecyclerView.apply {
            this@BeerListActivity.adapter.updateBeers(beers)
            smoothScrollToPosition(0)
            setVisible(true)
        }
    }

    override fun showNoData() {
        beerListNoResultsTv.setVisible(true)
    }

    override fun showError(error: Throwable) {
        snackbar.setText(getString(R.string.error_general, error.localizedMessage)).show()
    }
}