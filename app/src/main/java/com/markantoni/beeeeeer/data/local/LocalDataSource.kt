package com.markantoni.beeeeeer.data.local

import com.markantoni.beeeeeer.data.DataSource
import com.markantoni.beeeeeer.model.Beer

internal interface LocalDataSource : DataSource {
    fun saveFetchedBeers(beers: List<Beer>)
}