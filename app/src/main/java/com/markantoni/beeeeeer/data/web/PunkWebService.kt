package com.markantoni.beeeeeer.data.web

import com.markantoni.beeeeeer.model.Beer
import retrofit2.Call
import retrofit2.http.GET

internal interface PunkWebService {
    //?page=1&per_page=80 could consider making pagination, but is it needed for demo?
    @GET("v2/beers")
    fun fetchAllBeers(): Call<List<Beer>>
}