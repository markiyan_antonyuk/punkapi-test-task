package com.markantoni.beeeeeer.data.local.db

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.markantoni.beeeeeer.model.Beer

/** Table which contains ids of favorite beers. */
@Entity(tableName = "favorites")
internal class FavoriteStatus(@PrimaryKey val beerId: Int)

/** Extension field which returns [FavoriteStatus] from [Beer] */
internal val Beer.favoriteStatus: FavoriteStatus
    get() = FavoriteStatus(id)