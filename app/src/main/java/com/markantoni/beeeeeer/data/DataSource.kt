package com.markantoni.beeeeeer.data

import com.markantoni.beeeeeer.model.Beer

/**
 * Clean Architecture`s data source.
 * It should be located in separate module why most of classes are marked as package private.
 */
interface DataSource {
    fun fetchAllBeers(onSuccess: (List<Beer>) -> Unit, onError: (Throwable) -> Unit)
    fun fetchFavoriteBeers(onSuccess: (List<Beer>) -> Unit, onError: (Throwable) -> Unit)
    fun fetchFavoriteStatus(beer: Beer, callback: (Boolean) -> Unit)
    fun setFavorite(beer: Beer)
    fun removeFromFavorites(beer: Beer)
}