package com.markantoni.beeeeeer.data

import android.content.Context
import android.net.ConnectivityManager
import com.markantoni.beeeeeer.data.local.LocalDataSource
import com.markantoni.beeeeeer.data.local.LocalDataSourceImpl
import com.markantoni.beeeeeer.data.web.WebDataSource
import com.markantoni.beeeeeer.model.Beer

object BeerDataSource : DataSource {
    private lateinit var connectivityManager: ConnectivityManager
    private lateinit var webDataSource: WebDataSource
    private lateinit var localDataSource: LocalDataSource

    fun init(context: Context) {
        connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        webDataSource = WebDataSource()
        localDataSource = LocalDataSourceImpl(context)
    }

    override fun fetchAllBeers(onSuccess: (List<Beer>) -> Unit, onError: (Throwable) -> Unit) {
        if (isOnline()) {
            webDataSource.fetchAllBeers({
                localDataSource.saveFetchedBeers(it)
                onSuccess(it)
            }, onError)
        } else {
            localDataSource.fetchAllBeers(onSuccess, onError)
        }
    }

    override fun fetchFavoriteBeers(onSuccess: (List<Beer>) -> Unit, onError: (Throwable) -> Unit) {
        localDataSource.fetchFavoriteBeers(onSuccess, onError)
    }

    override fun setFavorite(beer: Beer) = localDataSource.setFavorite(beer)

    override fun removeFromFavorites(beer: Beer) = localDataSource.removeFromFavorites(beer)

    override fun fetchFavoriteStatus(beer: Beer, callback: (Boolean) -> Unit) = localDataSource.fetchFavoriteStatus(beer, callback)

    private fun isOnline() = connectivityManager.activeNetworkInfo?.isAvailable ?: false
}