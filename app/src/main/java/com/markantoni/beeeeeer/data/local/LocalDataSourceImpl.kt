package com.markantoni.beeeeeer.data.local

import android.arch.persistence.room.Room
import android.content.Context
import android.os.Handler
import android.os.Looper
import com.markantoni.beeeeeer.data.local.db.BeerDatabase
import com.markantoni.beeeeeer.data.local.db.favoriteStatus
import com.markantoni.beeeeeer.model.Beer
import java.util.concurrent.Executors

internal class LocalDataSourceImpl(context: Context) : LocalDataSource {
    private val database = Room.databaseBuilder(context, BeerDatabase::class.java, "beer-database").build()
    private val executor = Executors.newSingleThreadExecutor()
    private val handler = Handler(Looper.getMainLooper())

    override fun saveFetchedBeers(beers: List<Beer>) {
        executor.submit { database.getBeerDao().saveBeers(beers) }
    }

    override fun fetchAllBeers(onSuccess: (List<Beer>) -> Unit, onError: (Throwable) -> Unit) {
        executor.submit {
            val beers = database.getBeerDao().selectAllBeers()
            handler.post { onSuccess(beers) }
        }
    }

    override fun fetchFavoriteBeers(onSuccess: (List<Beer>) -> Unit, onError: (Throwable) -> Unit) {
        executor.submit {
            val favoriteIds = database.getFavoriteStatusDao().selectFavorites().map { it.beerId }
            val beers = database.getBeerDao().selectBeers(favoriteIds)
            handler.post { onSuccess(beers) }
        }
    }

    override fun fetchFavoriteStatus(beer: Beer, callback: (Boolean) -> Unit) {
        executor.submit {
            val favorite = database.getFavoriteStatusDao().selectFavorite(beer.id) != null
            handler.post { callback(favorite) }
        }
    }

    override fun setFavorite(beer: Beer) {
        executor.submit { database.getFavoriteStatusDao().addFavorite(beer.favoriteStatus) }
    }

    override fun removeFromFavorites(beer: Beer) {
        executor.submit { database.getFavoriteStatusDao().removeFavorite(beer.favoriteStatus) }
    }
}