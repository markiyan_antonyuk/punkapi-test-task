package com.markantoni.beeeeeer.data.local.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.markantoni.beeeeeer.model.Beer

@Database(entities = [Beer::class, FavoriteStatus::class], version = 1)
internal abstract class BeerDatabase : RoomDatabase() {
    abstract fun getBeerDao(): BeerDao
    abstract fun getFavoriteStatusDao(): FavoriteStatusDao
}
