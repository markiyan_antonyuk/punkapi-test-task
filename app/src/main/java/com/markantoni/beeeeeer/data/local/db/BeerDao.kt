package com.markantoni.beeeeeer.data.local.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.markantoni.beeeeeer.model.Beer

@Dao
internal interface BeerDao {
    @Query("SELECT * FROM beers")
    fun selectAllBeers(): List<Beer>

    @Query("SELECT * FROM beers WHERE id IN (:ids)")
    fun selectBeers(ids: List<Int>): List<Beer>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveBeers(beers: List<Beer>)
}
