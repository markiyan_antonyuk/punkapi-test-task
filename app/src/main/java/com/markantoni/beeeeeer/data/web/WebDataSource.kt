package com.markantoni.beeeeeer.data.web

import com.markantoni.beeeeeer.data.DataSource
import com.markantoni.beeeeeer.model.Beer
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

internal class WebDataSource : DataSource {
    private val webService: PunkWebService

    init {
        val retrofit = Retrofit.Builder()
                .baseUrl("https://api.punkapi.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        webService = retrofit.create(PunkWebService::class.java)
    }

    override fun fetchAllBeers(onSuccess: (List<Beer>) -> Unit, onError: (Throwable) -> Unit) {
        webService.fetchAllBeers().enqueue(object : Callback<List<Beer>> {
            override fun onResponse(call: Call<List<Beer>>, response: Response<List<Beer>>) {
                response.body()?.let { onSuccess(it) } ?: onError(NullPointerException("Response is null"))
            }

            override fun onFailure(call: Call<List<Beer>>, t: Throwable) = onError(t)
        })
    }

    //not used, backend has no favorite state
    override fun fetchFavoriteBeers(onSuccess: (List<Beer>) -> Unit, onError: (Throwable) -> Unit) = Unit
    override fun setFavorite(beer: Beer) = Unit
    override fun removeFromFavorites(beer: Beer) = Unit
    override fun fetchFavoriteStatus(beer: Beer, callback: (Boolean) -> Unit) = Unit
}