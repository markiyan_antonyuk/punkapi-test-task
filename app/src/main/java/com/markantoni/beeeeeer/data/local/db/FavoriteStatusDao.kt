package com.markantoni.beeeeeer.data.local.db

import android.arch.persistence.room.*

@Dao
internal interface FavoriteStatusDao {
    @Query("SELECT * FROM favorites")
    fun selectFavorites(): List<FavoriteStatus>

    @Query("SELECT * FROM favorites WHERE beerId = :beerId")
    fun selectFavorite(beerId: Int): FavoriteStatus?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addFavorite(favoriteStatus: FavoriteStatus)

    @Delete
    fun removeFavorite(favoriteStatus: FavoriteStatus)
}