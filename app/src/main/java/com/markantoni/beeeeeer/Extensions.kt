package com.markantoni.beeeeeer

import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

/** Usable extensions. */
fun View.setVisible(visible: Boolean) {
    visibility = if (visible) View.VISIBLE else View.GONE
}

fun ImageView.loadImage(url: String) {
    Glide.with(context)
            .load(url)
            .apply(RequestOptions().placeholder(R.drawable.ic_beer).centerInside())
            .into(this)
}

/** Toggle [MenuItem] and return if it was checked before. */
fun MenuItem.toggle(): Boolean {
    val checked = isChecked
    isChecked = !checked
    return checked
}